import { SportStranaPage } from './app.po';

describe('sport-strana App', () => {
  let page: SportStranaPage;

  beforeEach(() => {
    page = new SportStranaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
