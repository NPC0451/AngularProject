import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes }   from '@angular/router';

import { EventsComponent} from './events/events.component';
import { ProfileComponent} from './profile/profile.component';
import { AppComponent } from './app.component';
const appRoutes: Routes = [
  { path: 'events', component: EventsComponent },
  { path: 'profile', component: ProfileComponent }
];
@NgModule({
  declarations: [
    AppComponent,
    EventsComponent,
    ProfileComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
